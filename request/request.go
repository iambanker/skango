package request

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"skango/geo"
	"time"
)

type TileFile string
type MapType string

const (
	authUrl           string   = "https://spaceknow.auth0.com/oauth/ro"
	krakenUrl         string   = "https://spaceknow-kraken.appspot.com"
	KrakenAircraftPNG TileFile = "aircraft.png"
	KrakenImageryPNG  TileFile = "truecolor.png"
	KrakenImageryMap  MapType  = "imagery"
	KrakenAircraftMap MapType  = "aircraft"
	KrakenTileLimit   int      = 16
	ragnarUrl         string   = "https://spaceknow-imagery.appspot.com"
	taskingStatusUrl  string   = "https://spaceknow-tasking.appspot.com/tasking/get-status"
	// Token should be valid for 10 hours, use shorter interval as conservative measure
	tokenValidSecs float64 = 35000
)

var taskingFiniteStatus = map[string]bool{
	"FAILED":   true,
	"RESOLVED": true,
}
var mapTypes = map[MapType]bool{
	KrakenImageryMap:  true,
	KrakenAircraftMap: true,
}

type Header map[string]string
type AsyncRequest interface {
	InitUrl() string
	RetrieveUrl() string
	InitQuery() []byte
}
type Request interface {
	Url() string
	Query() []byte
}
type AsyncResult struct {
	PipelineId string
	Response   []byte
}
type SearchRequest struct {
	Provider      string     `json:"provider"`
	Dataset       string     `json:"dataset"`
	StartDatetime string     `json:"startDatetime"`
	EndDatetime   string     `json:"endDatetime"`
	Extent        geo.Extent `json:"extent"`
}

func NewSearchRequest(provider, dataset, startDt, endDt string, extent geo.Extent) SearchRequest {
	req := SearchRequest{
		Extent:        extent,
		Provider:      provider,
		Dataset:       dataset,
		StartDatetime: startDt,
		EndDatetime:   endDt,
	}
	return req
}

func (this SearchRequest) InitQuery() []byte {
	parsed, err := json.Marshal(this)
	if err != nil {
		panic(err)
	}
	return parsed
}

func (this SearchRequest) InitUrl() string {
	return ragnarUrl + "/imagery/search/initiate"
}

func (this SearchRequest) RetrieveUrl() string {
	return ragnarUrl + "/imagery/search/retrieve"
}

func ParseRagnarScenes(results [][]byte) []geo.Scene {
	scenes := make([]geo.Scene, len(results))
	for i, result := range results {
		scene := geo.Scene{}
		err := json.Unmarshal(result, &scene)
		if err != nil {
			panic(err)
		}
		scenes[i] = scene
	}
	return scenes
}

type TileRequest struct {
	SceneId string     `json:"sceneId"`
	Tiles   []geo.Tile `json:"tiles"`
	MapType MapType    `json:"-"`
}

func NewTileRequest(sceneId string, tiles []geo.Tile, mapType MapType) TileRequest {
	if !mapTypes[mapType] {
		panic(fmt.Sprintf("Map type %s is not valid option.", mapType))
	}
	req := TileRequest{
		SceneId: sceneId,
		Tiles:   tiles,
		MapType: mapType,
	}
	return req
}

func (this TileRequest) InitQuery() []byte {
	parsed, err := json.Marshal(this)
	if err != nil {
		panic(err)
	}
	return parsed

}
func (this TileRequest) InitUrl() string {
	return krakenUrl + fmt.Sprintf("/kraken/release/%s/tiles/initiate", this.MapType)
}

func (this TileRequest) RetrieveUrl() string {
	return krakenUrl + fmt.Sprintf("/kraken/release/%s/tiles/retrieve", this.MapType)
}

type KrakenTile struct {
	MapId   string     `json:"mapId"`
	MaxZoom int        `json:"maxZoom"`
	Tiles   []geo.Tile `json:"Tiles"`
}

func ParseKrakenTile(result []byte) KrakenTile {
	var krakenTile KrakenTile
	err := json.Unmarshal(result, &krakenTile)
	if err != nil {
		panic(err)
	}
	return krakenTile
}

type GridRequest struct {
	MapId, PolygonId string
	Filename         TileFile
	Tile             geo.Tile
}

func NewGridRequest(MapId, PolygonId string, Filename TileFile, Tile geo.Tile) GridRequest {
	request := GridRequest{MapId, PolygonId, Filename, Tile}
	return request
}

func (this GridRequest) Url() string {
	return krakenUrl + fmt.Sprintf("/kraken/grid/%s/%s/%d/%d/%d/%s", this.MapId, this.PolygonId,
		this.Tile.Zoom, this.Tile.X, this.Tile.Y, this.Filename)
}

func (this GridRequest) Query() []byte {
	var body []byte
	return body
}

type retrieveQuery struct {
	PipelineId string `json:"pipelineId"`
	Cursor     string `json:"cursor,omitempty"`
}

// Session can be used to execute queries against Spaceknow APIs. The session stores and reuse authorization token thus it should be passed by reference to prevent a creation of session copy which might lead to unnecessary re-authentication.
type AuthSession struct {
	username string
	password string
	token    string
	token_ts time.Time
}

func NewAuthSession(username, password string) *AuthSession {
	authSession := AuthSession{}
	authSession.username = username
	authSession.password = password
	// Authenticate immediately to ensure the token will be reused even in case the session is passed by value instead as reference. Moreover, this will ensure that execution fails asap as invalid credentials will lead to an error right when the session is being initialized.
	authSession.authenticate()
	return &authSession
}

// Handle API authentication. Authenticate against API Auth endpoint and store authorization token if previous token expired or token  was not acquired yet.
func (this *AuthSession) authenticate() {
	t := time.Now()
	elaps := t.Sub(this.token_ts)
	if this.token != "" && elaps.Seconds() < tokenValidSecs {
		return
	}
	// TODO: make this thread safe
	fmt.Println("Requesting new authorization token...")
	bodyMap := make(map[string]string)
	bodyMap["client_id"] = "Qqdwx21Ocn2Yj0q3ANunnXVp3IYE0uhW"
	bodyMap["username"] = this.username
	bodyMap["password"] = this.password
	bodyMap["connection"] = "Username-Password-Authentication"
	bodyMap["grant_type"] = "password"
	bodyMap["scope"] = "openid"
	body := toJson(bodyMap)
	rawResp := rawPostRequest(authUrl, Header{}, body)
	resp := fromJson(rawResp)

	this.token = resp["id_token"].(string)
	this.token_ts = time.Now()
}

func (this *AuthSession) getAuthHeader() Header {
	this.authenticate()
	header := make(Header)
	header["Authorization"] = fmt.Sprintf("Bearer %s", this.token)
	return header
}

// Initiate pipeline and retrieve the its first result. For endpoints which return paginated result see RetrievePaginatedResults.
func (this *AuthSession) FireAsyncRequest(asyncReq AsyncRequest) AsyncResult {
	header := this.getAuthHeader()
	rawResp := rawPostRequest(asyncReq.InitUrl(), header, asyncReq.InitQuery())
	initResp := fromJson(rawResp)
	pipelineId := initResp["pipelineId"].(string)

	pipelineRequest := make(map[string]string)
	pipelineRequest["pipelineId"] = pipelineId
	// Can be used both for Tasking API and Retrieve endpoint
	pipelineQuery, err := json.Marshal(pipelineRequest)
	if err != nil {
		panic(err)
	}

	err = waitPipelineFinish(pipelineId, pipelineQuery, header)
	// TODO: implement better handling of failed pipelines
	if err != nil {
		panic(err)
	}
	rawResp = rawPostRequest(asyncReq.RetrieveUrl(), header, pipelineQuery)
	return AsyncResult{PipelineId: pipelineId, Response: rawResp}
}

// Fire request against API endpoint and return its result.
func (this *AuthSession) FireRequest(req Request) []byte {
	header := this.getAuthHeader()
	return rawPostRequest(req.Url(), header, req.Query())
}

// Retrieve results of paginated API endpoint.
func (this *AuthSession) RetrievePaginatedResults(pipeResult AsyncResult, url string) [][]byte {
	header := this.getAuthHeader()
	parsedResponse := fromJson(pipeResult.Response)
	cursor := parsedResponse["cursor"]
	results := parsedResponse["results"].([]interface{})
	var pipelineResults [][]byte
	for _, result := range results {
		rawResult := toJson(result)
		pipelineResults = append(pipelineResults, rawResult)
	}
	// Retrieve the rest of  paginated results
	query := retrieveQuery{PipelineId: pipeResult.PipelineId}
retrieveLoop:
	for {
		switch cursor.(type) {
		case string:
			// Set cursor for next query
			query.Cursor = cursor.(string)
		case nil:
			break retrieveLoop
		default:
			panic(fmt.Sprintf("Invalid cursor %v of type %T.", cursor, cursor))
		}
		/* Query has to be converted to bytes for each time to reflect
		eventual new value of cursor */
		rawResp := rawPostRequest(url, header, toJson(query))
		retrieveResp := fromJson(rawResp)
		results := retrieveResp["results"].([]interface{})
		for _, result := range results {
			rawResult := toJson(result)
			pipelineResults = append(pipelineResults, rawResult)
		}
		cursor = retrieveResp["cursor"]
	}
	return pipelineResults
}

// Function to ensure that initiated pipeline reach a final state.
func waitPipelineFinish(pipelineId string, query []byte, header Header) error {
	for {
		fmt.Printf("Querying status of pipeline %s.\n", pipelineId)
		rawResp := rawPostRequest(taskingStatusUrl, header, query)
		taskingResp := fromJson(rawResp)
		pipelineStatus := taskingResp["status"].(string)
		if taskingFiniteStatus[pipelineStatus] {
			switch pipelineStatus {
			case "FAILED":
				return errors.New(fmt.Sprintf("Pipeline %s failed.", pipelineId))
			case "RESOLVED":
				return nil
			default:
				panic(fmt.Sprintf("Invalid pipeline status %s.", pipelineStatus))
			}
		} else {
			time.Sleep(2 * time.Second)
		}
	}
}

// Fire POST request against specified endpoint and return raw response.
func rawPostRequest(url string, header Header, body []byte) []byte {
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(body))
	if err != nil {
		panic(err)
	}
	req.Header.Set("Content-Type", "application/json")
	for h, value := range header {
		req.Header.Set(h, value)
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	// TODO: Handle errors in better way than just failing the whole process.
	// Handle client errors
	if resp.StatusCode >= 400 && resp.StatusCode < 500 {
		panic(fmt.Sprintf("Client Error %s occured: %s.", resp.Status,
			string(respBody)))
	}
	// Handle server errors
	if resp.StatusCode >= 500 && resp.StatusCode < 600 {
		panic(fmt.Sprintf("Server Error %s occured: %s.\n", resp.Status,
			string(respBody)))
	}
	return respBody
}

// Function to parse JSON API response to map.
func fromJson(obj []byte) map[string]interface{} {
	var response map[string]interface{}
	err := json.Unmarshal(obj, &response)
	if err != nil {
		panic(err)
	}
	return response
}

// Function to serialize interface to JSON accepted by API endpoints.
func toJson(obj interface{}) []byte {
	parsed, err := json.Marshal(obj)
	if err != nil {
		panic(err)
	}
	return parsed
}
