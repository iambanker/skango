# Skango

``skango`` is command line tool written in Go which can be used to interact
with SpaceKnow ecosystem.

## Usage

Using ``skango`` requires that ``Go`` is successfully installed on your machine.

Firstly check that tests pass:

```bash
# cd to root dir before running the command
$ go test ./...
```

Then build ``skango``:
```bash
$ go build
```

Finally run analysis and download the imagery together with prediction mask:
```bash
$ ./skango -credentials=credentials.json ---output=. \
           -provider=dg -dataset=wfs-wms -geojson=aoi.geojson \
           -from=2016-01 -to=2016-02
```

* ``credentials.json`` should contain valid credentials in following format:
```json
{
  "username": "john.doe@ceo.universe",
  "password": "you.cannot.brute.force.me"
}
```
These credentials are used for authentication.

* ``aoi.geojson`` should contain valid GeoJSON specifying area of interest.

By running  ``./skango -h`` you can display short help.

## Shortcommings

* current version supports only analysis of aircrafts
* many errors are not handled and single error causes the whole process to crash
* ``skango`` currently runs single threaded except for downloading of tiles
