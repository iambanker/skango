package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"image"
	"image/draw"
	"image/png"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"skango/geo"
	"skango/request"
	"time"
)

const (
	argDateFormat      string = "2006-01"
	fileDateTimeFormat string = "2006-01-02T15:04:05Z"
	dateTimeFormat     string = "2006-01-02 15:04:05"
	gridTileSize       int    = 256
)

var Providers = map[string]bool{"dg": true}
var Datasets = map[string]bool{"wfs-wms": true}

func main() {
	args := parseSkArgs()
	credentials := LoadCredentials(args.credentials)
	extent := geo.LoadExtent(args.geojson)
	fmt.Println("Analysing satellite imagery using SpaceKnow ecosystem...")

	sess := request.NewAuthSession(credentials.Username, credentials.Password)
	// Search available imagery
	searchReq := request.NewSearchRequest(
		args.provider, args.dataset, args.from.Format(dateTimeFormat),
		args.to.Format(dateTimeFormat), extent,
	)
	scenes := runSearchPipeline(sess, searchReq)
	// Run analysis using Kraken API
	analyses := runAnalyticsPipeline(sess, scenes, extent)
	storeAnalysesToPNG(analyses, args.output)
}

type skArgs struct {
	credentials, geojson, dataset, provider, output string
	from, to                                        time.Time
}

func parseSkArgs() skArgs {
	credentials := flag.String("credentials", "", "Path to credentials file.")
	geojson := flag.String("geojson", "", "Path to geojson file.")
	provider := flag.String("provider", "", "Name of imagery provider.")
	dataset := flag.String("dataset", "", "Name of imagery dataset.")
	output := flag.String("output", "", "Folder to store analysis results.")
	from := flag.String("from", "2000-01", "Date from which analyse imagery, inclusive.")
	to := flag.String("to", "2020-01", "Date to which analyse imagery, inclusive.")
	flag.Parse()

	if _, err := os.Stat(*credentials); os.IsNotExist(err) {
		panic(fmt.Sprintf("Credentials file specified by "+
			"\"%s\" does not exits.\n", *credentials))
	}
	if _, err := os.Stat(*geojson); os.IsNotExist(err) {
		panic(fmt.Sprintf("Geojson specified by \"%s\" does not exits.\n", *geojson))
	}
	if _, err := os.Stat(*output); os.IsNotExist(err) {
		panic(fmt.Sprintf("Output directory \"%s\" does not exits.\n", *output))
	}
	if !Providers[*provider] {
		panic(fmt.Sprintf("Provider \"%s\" is not valid.\n", *dataset))
	}
	if !Datasets[*dataset] {
		panic(fmt.Sprintf("Path \"%s\" is not valid.\n", *dataset))
	}

	fromTime, err := time.Parse(argDateFormat, *from)
	if err != nil {
		panic(err)
	}
	toTime, err := time.Parse(argDateFormat, *to)
	if err != nil {
		panic(err)
	}

	return skArgs{geojson: *geojson, provider: *provider, dataset: *dataset,
		credentials: *credentials, from: fromTime, to: toTime, output: *output}
}

type Credentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// Load credentials required for authentication
func LoadCredentials(filename string) Credentials {
	var credentials Credentials
	credentialsFile, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(credentialsFile, &credentials)
	if err != nil {
		panic(err)
	}

	if credentials.Username == "" || credentials.Password == "" {
		panic("Invalid credentials file, must contain both username and password.")
	}
	return credentials
}

// Search for available imagery.
func runSearchPipeline(sess *request.AuthSession, req request.SearchRequest) []geo.Scene {
	results := sess.FireAsyncRequest(req)
	ragnarResults := sess.RetrievePaginatedResults(results, req.RetrieveUrl())
	return request.ParseRagnarScenes(ragnarResults)
}

// Run Kraken analysis for searched scenes.
func runAnalyticsPipeline(sess *request.AuthSession, scenes []geo.Scene, extent geo.Extent) []Analysis {
	analyses := make([]Analysis, len(scenes))
	for i, scene := range scenes {
		tiles := extent.ToTiles(scene.FinestRes())
		analyses[i] = analyseScene(sess, scene, tiles)
	}
	return analyses
}

func analyseScene(sess *request.AuthSession, scene geo.Scene, tiles []geo.Tile) Analysis {
	analysis := NewAnalysis(scene)
	// It's necessary to fire requests in batches due to tile limit on API side
	for start := 0; start < len(tiles); start += request.KrakenTileLimit {
		end := start + request.KrakenTileLimit
		if end > len(tiles) {
			end = len(tiles)
		}
		requestTiles := tiles[start:end]
		// Request imagery
		imageryRequest := request.NewTileRequest(
			scene.SceneId, requestTiles, request.KrakenImageryMap,
		)
		imageryResult := sess.FireAsyncRequest(imageryRequest)
		tileResponse := request.ParseKrakenTile(imageryResult.Response)
		collectedTiles := collectTiles(sess, tileResponse, request.KrakenImageryPNG)
		analysis.Imagery = append(analysis.Imagery, collectedTiles...)
		// Request analysis
		analysisRequest := request.NewTileRequest(
			scene.SceneId, requestTiles, request.KrakenAircraftMap,
		)
		analysisResult := sess.FireAsyncRequest(analysisRequest)
		tileAnalysisResponse := request.ParseKrakenTile(analysisResult.Response)
		collectedAnalysisTiles := collectTiles(sess, tileAnalysisResponse, request.KrakenAircraftPNG)
		analysis.Mask = append(analysis.Mask, collectedAnalysisTiles...)
	}
	return analysis
}

// Collect tile generated by Kraken release endpoint using Kraken grid endpoint
func collectTiles(sess *request.AuthSession, tileResponse request.KrakenTile, tileFile request.TileFile) []GridTile {
	tiles := tileResponse.Tiles
	channel := make(chan GridTile)
	for _, tile := range tiles {
		req := request.NewGridRequest(
			tileResponse.MapId, "-", tileFile, tile,
		)
		go func(req request.GridRequest) {
			response := sess.FireRequest(req)
			buffer := bytes.NewBuffer(response)
			img, err := png.Decode(buffer)
			if err != nil {
				panic(err)
			}
			channel <- GridTile{img, req.Tile}

		}(req)
	}
	// Retrieve goroutine grid results
	gridTiles := make([]GridTile, len(tiles))
	for i := 0; i < len(tiles); i++ {
		gridTiles[i] = <-channel
	}
	return gridTiles
}

func storeAnalysesToPNG(analyses []Analysis, outputDir string) {
	for _, analysis := range analyses {
		analysis.storeToPNG(outputDir)
	}
}

type Analysis struct {
	Imagery []GridTile
	Mask    []GridTile
	Scene   geo.Scene
}

func NewAnalysis(scene geo.Scene) Analysis {
	imagery := make([]GridTile, 0)
	mask := make([]GridTile, 0)
	analysis := Analysis{imagery, mask, scene}
	return analysis
}

func (this Analysis) storeToPNG(outputDir string) {
	parsedDt, err := time.Parse(dateTimeFormat, this.Scene.Datetime)
	outputFilename := fmt.Sprintf("%s_%s.png", this.Scene.SceneId, parsedDt.Format(fileDateTimeFormat))
	outputFile, _ := filepath.Abs(path.Join(outputDir, outputFilename))
	fmt.Printf("Storing analysis to %s...\n", outputFile)
	file, err := os.Create(outputFile)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	minX, minY, maxX, maxY := this.getCornerTilePos()
	img := image.NewRGBA(image.Rect(0, 0, (maxX-minX+1)*gridTileSize, (maxY-minY+1)*gridTileSize))
	for _, gridTile := range this.Imagery {
		draw.Draw(img, gridTile.getPixelBounds(minX, minY), gridTile.Image,
			image.Point{0, 0}, draw.Src)
	}
	for _, gridTile := range this.Mask {
		draw.Draw(img, gridTile.getPixelBounds(minX, minY), gridTile.Image,
			image.Point{0, 0}, draw.Over)
	}
	err = png.Encode(file, img)
	if err != nil {
		panic(err)
	}
}

// Find positions of top left (minX, minY) and bottom right (maxX, maxY) tiles in tile space.
func (this Analysis) getCornerTilePos() (int, int, int, int) {
	// Init to values of first tile
	tile := this.Imagery[0].Tile
	minX, minY, maxX, maxY := tile.X, tile.Y, tile.X, tile.Y
	// TODO: support tiles with different zoom level. Algorithm for concatenating
	// tile images have to be adjusted too.
	expectedTileZoom := tile.Zoom
	// Iterate over both imagery and mask tiles
	gridTiles := append(this.Imagery[1:], this.Mask...)
	for _, gridTile := range gridTiles {
		tile := gridTile.Tile
		// Check all tiles have the same zoom level
		if tile.Zoom != expectedTileZoom {
			panic(fmt.Sprintf("Analysis contains tiles with different zoom levels %d and %d.",
				tile.Zoom, expectedTileZoom))
		}
		if tile.X < minX {
			minX = tile.X
		}
		if tile.X > maxX {
			maxX = tile.X
		}
		if tile.Y < minY {
			minY = tile.Y
		}
		if tile.Y > maxY {
			maxY = tile.Y
		}
	}
	if minX > maxX || minY > maxY {
		panic(fmt.Sprintf("Boundary tiles of analysis have invalid positions minX=%d, minY=%d, maxX=%d, maxY=%d).",
			minX, minY, maxX, maxY))
	}
	return minX, minY, maxX, maxY
}

type GridTile struct {
	Image image.Image
	Tile  geo.Tile
}

// Function determines position of grid tile in pixel space with respect to base
// coordinates in Web Mercator tile space. Such information is useful when
// concatenating multiple grid tiles into single image.
func (this GridTile) getPixelBounds(baseXPos, baseYPos int) image.Rectangle {
	bounds := this.Image.Bounds()
	// According to docs grid tile image should have size defined by gridTileSize,
	// see https://docs.spaceknow.com/api/kraken.html#map-and-tiling.
	if bounds.Dx() != gridTileSize || bounds.Dy() != gridTileSize {
		panic(fmt.Sprintf("Tile size %dx%d differs from expected tile size %dx%d.",
			bounds.Dx(), bounds.Dy(), gridTileSize, gridTileSize))
	}
	xStartPos := (this.Tile.X - baseXPos) * gridTileSize
	yStartPos := (this.Tile.Y - baseYPos) * gridTileSize
	xEndPos := xStartPos + gridTileSize
	yEndPos := yStartPos + gridTileSize
	return image.Rect(xStartPos, yStartPos, xEndPos, yEndPos)
}
