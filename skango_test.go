package main

import (
	"image"
	"skango/geo"
	"testing"
)

func TestGetCornerTilePos(t *testing.T) {
	analysis := NewAnalysis(geo.Scene{})
	fakeImg := image.NewRGBA(image.Rect(0, 0, 1, 1))
	analysis.Imagery = []GridTile{
		GridTile{fakeImg, geo.Tile{19, 0, 1}},
		GridTile{fakeImg, geo.Tile{19, 4, 3}},
		GridTile{fakeImg, geo.Tile{19, 2, 7}},
	}
	analysis.Mask = []GridTile{
		GridTile{fakeImg, geo.Tile{19, 2, 3}},
		GridTile{fakeImg, geo.Tile{19, 5, 5}},
	}
	minX, minY, maxX, maxY := analysis.getCornerTilePos()
	if minX != 0 || minY != 1 || maxX != 5 || maxY != 7 {
		t.Errorf("Expected position (%d, %d, %d, %d), got (%d, %d, %d, %d)", 0, 1, 5, 7, minX, minY, maxX, maxY)
	}
}

func TestGetPixelBounds(t *testing.T) {
	fakeImg := image.NewRGBA(image.Rect(0, 0, 256, 256))
	gridTile := GridTile{fakeImg, geo.Tile{19, 2, 1}}
	bounds := gridTile.getPixelBounds(1, 1)
	expectedBounds := image.Rect(256, 0, 512, 256)
	if bounds != expectedBounds {
		t.Errorf("Expected bounds %v, got %v.", expectedBounds, bounds)
	}

}
