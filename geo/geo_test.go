package geo

import "testing"

func TestMaxZoom(t *testing.T) {
	expectedMaxZoom := 19.0
	maxZoom := getMaxZoom(37.785910776551354, 0.3)
	if maxZoom != expectedMaxZoom {
		t.Errorf("MaxZoom was incorrect, got: %d, want: %d.", maxZoom, expectedMaxZoom)
	}
}
