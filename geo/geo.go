package geo

import (
	"encoding/json"
	"fmt"
	"github.com/paulmach/go.geojson"
	"io/ioutil"
	"math"
)

type Bbox struct {
	Xmin, Xmax, Ymin, Ymax float64
}

type Centroid struct {
	Lon, Lat float64
}
type Extent struct {
	featureCollection geojson.FeatureCollection
	bbox              Bbox
}

func (this Extent) MarshalJSON() ([]byte, error) {
	bytes, err := this.featureCollection.MarshalJSON()
	if err != nil {
		panic(err)
	}
	return bytes, nil
}

func (this Extent) getCentroid() Centroid {
	lon := (this.bbox.Xmin + this.bbox.Xmax) / 2
	lat := (this.bbox.Ymin + this.bbox.Ymax) / 2
	c := Centroid{Lon: lon, Lat: lat}
	return c
}

func (this Extent) ToTiles(resolution float64) []Tile {
	var tiles []Tile
	centroid := this.getCentroid()
	maxZoom := getMaxZoom(centroid.Lat, resolution)
	northWestTile := LonLatToTile(this.bbox.Xmin, this.bbox.Ymax,
		maxZoom)
	southEastTile := LonLatToTile(this.bbox.Xmax, this.bbox.Ymin,
		maxZoom)
	baseX := northWestTile.X
	baseY := northWestTile.Y
	xTiles := southEastTile.X - northWestTile.X
	yTiles := southEastTile.Y - northWestTile.Y
	for xOffset := 0; xOffset <= xTiles; xOffset++ {
		for yOffset := 0; yOffset <= yTiles; yOffset++ {
			tile := Tile{
				X:    baseX + xOffset,
				Y:    baseY + yOffset,
				Zoom: int(maxZoom),
			}
			tiles = append(tiles, tile)
		}
	}

	return tiles
}

// Load extent containing analysis AOI.
func LoadExtent(filename string) Extent {
	rawJson, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	fc, err := geojson.UnmarshalFeatureCollection(rawJson)
	if err != nil {
		panic(err)
	}
	extent := Extent{}
	extent.featureCollection = *fc
	bbox := getBboxFromFc(*fc)
	extent.bbox = bbox
	return extent
}

// Represent scene information. Follows schema defined by Ragnar search endpoint.
type Scene struct {
	SceneId      string           `json:"sceneId"`
	Provider     string           `json:"provider"`
	Dataset      string           `json:"dataset"`
	Satellite    string           `json:"satellite"`
	Datetime     string           `json:"datetime"`
	CrsEpsg      int              `json:"crsEpsg,omitempty"`
	Footprint    geojson.Geometry `json:"footprint"`
	OffNadir     float64          `json:"offNadir,omitempty"`
	SunElevation float64          `json:"sunElevation,omitempty"`
	SunAzimuth   float64          `json:"sunAzimuth,omitempty"`
	Bands        []Band           `json:bands"`
}

type Band struct {
	Names                  []string `json:"names"`
	BitDepth               int      `json:"bitDepth,omitempty"`
	Gsd                    float64  `json:"gsd,omitempty"`
	PixelSizeX             float64  `json:"pixelSizeX,omitempty"`
	PixelSizeY             float64  `json:"pixelSizeY,omitempty"`
	CrsOriginX             float64  `json:"crsOriginX,omitempty"`
	CrsOriginY             float64  `json:"crsOriginY,omitempty"`
	ApproximateResolutionX float64  `json:"approximateResolutionX"`
	ApproximateResolutionY float64  `json:"approximateResolutionY"`
}

func (this Scene) FinestRes() float64 {
	finestRes := 15.0
	for _, band := range this.Bands {
		if band.ApproximateResolutionX < finestRes {
			finestRes = band.ApproximateResolutionX
		}
		if band.ApproximateResolutionY < finestRes {
			finestRes = band.ApproximateResolutionY
		}
	}
	return finestRes
}

// Object definining its position in tile space.
type Tile struct {
	Zoom, X, Y int
}

func (this Tile) MarshalJSON() ([]byte, error) {
	array := []int{this.Zoom, this.X, this.Y}
	return json.Marshal(array)
}

func (this *Tile) UnmarshalJSON(b []byte) error {
	var parsed []int
	err := json.Unmarshal(b, &parsed)
	if err != nil {
		panic(err)
	}
	this.Zoom = parsed[0]
	this.X = parsed[1]
	this.Y = parsed[2]
	return nil
}

func getMaxZoom(lattitude, resolution float64) float64 {
	numerator := 4 * math.Pow(10, 7) * math.Cos(lattitude*math.Pi/180)
	denominator := 265 * resolution
	maxZoom := math.Ceil(math.Log2(numerator / denominator))
	return maxZoom
}

// Calculate bounding box for given extent.
func getBboxFromFc(fc geojson.FeatureCollection) Bbox {
	// Init to maximum opposite values, golang cannot calculate max from array :(
	xmin, xmax, ymin, ymax := 180.0, -180.0, 90.0, -90.0
	for _, feature := range fc.Features {
		switch feature.Geometry.Type {
		case geojson.GeometryPolygon:
			break
		default:
			panic(fmt.Sprintf("Feature collection type %s is not supported", feature.Geometry.Type))
		}
		coords := feature.Geometry.Polygon[0]
		for _, coord := range coords {
			x := coord[0]
			y := coord[1]
			if x < xmin {
				xmin = x
			}
			if x > xmax {
				xmax = x
			}
			if y < ymin {
				ymin = y
			}
			if y > ymax {
				ymax = y
			}
		}
	}
	bbox := Bbox{Xmin: xmin, Xmax: xmax, Ymin: ymin, Ymax: ymax}
	return bbox
}

func LonLatToTile(lon, lat, zoom float64) Tile {
	latRad := lat * math.Pi / 180.0
	n := math.Pow(2.0, zoom)
	xtile := int((lon + 180.0) / 360.0 * n)
	ytile := int((1.0 - math.Log(math.Tan(latRad)+(1.0/math.Cos(latRad)))/math.Pi) / 2.0 * n)
	tile := Tile{X: xtile, Y: ytile, Zoom: int(zoom)}
	return tile

}
